//
// Created by babbu on 26-04-2018.
//

#include "book_node.h"
book_node :: book_node(book &b)
{
    book_data = b;
    next = nullptr;
    freq = 1;
}
book_node ::book_node() {
    next = nullptr;
    freq = 0;
}
book book_node :: getbook_data()
{
    return book_data;
}
unsigned book_node :: getfreq()
{
    return freq;
}
void book_node:: setfreq(const unsigned &f)
{
    freq = f;
}
ostream& operator << (ostream &out,  book_node &b)
{
    out<<b.getbook_data();
    out<<b.getfreq()<<endl;
    return out;
}
void book_node :: save_to_file(ostream &out)
{
    out<<book_data.gettitle()<<endl;
    out<<book_data.getISBN()<<endl;
    out<<book_data.getauthor_name()<<endl;
    out<<freq;
}