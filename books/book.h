//
// Created by babbu on 26-04-2018.
//

#ifndef LIB_MANG_PROJECT_V2_BOOK_H
#define LIB_MANG_PROJECT_V2_BOOK_H

#include <iostream>
using namespace std;

class book {
    string ISBN;
    string title;
    string author_name;
public:
    book();
    book(string s1,string s2,string s3):ISBN(s1),author_name(s2),title(s3){}
    string getISBN();
    string gettitle();
    string getauthor_name();
    friend istream& operator >> (istream &, book &);
    friend ostream& operator << (ostream &, const book &);
    void operator = (const book &);
    void read_from_file(istream &);
};


#endif //LIB_MANG_PROJECT_V2_BOOK_H
