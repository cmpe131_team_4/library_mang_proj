//
// Created by babbu on 26-04-2018.
//
// LIST OF THE BOOKS IN THE LIBRARY
#ifndef LIB_MANG_PROJECT_V2_BOOK_DATABASE_H
#define LIB_MANG_PROJECT_V2_BOOK_DATABASE_H

#include "book_node.h"

class book_database {
protected:
    book_node *head;
    int number_of_books;
public:
    book_database();
    ~book_database();
    void destroy_books();
    void add_book(book &);
    void remove_book(book &);
    bool isEmpty();
    book get_book_data(string) ;
    bool search(string );
    friend ostream& operator << (ostream &, book_database &);
    void save_to_file(ostream &);
};


#endif //LIB_MANG_PROJECT_V2_BOOK_DATABASE_H
