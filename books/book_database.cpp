//
// Created by babbu on 26-04-2018.
//
// LIST OF THE BOOKS IN THE LIBRARY
#include "book_database.h"

book_database ::book_database() {
    head = nullptr;
    number_of_books = 0;
}
book_database ::~book_database() {
    destroy_books();
}
void book_database ::destroy_books() {
    book_node *temp;
    while(head!= nullptr)
    {
        temp = head;
        head = head->next;
        delete  temp;
    }
}
bool book_database ::isEmpty() {
    if(number_of_books==0)
        return true;
    else
        return false;
}

void book_database ::add_book( book &b) {
    book_node *temp;
    if(head == nullptr)
    {
        head = new book_node(b);
        number_of_books++;
        return;
    }
    else
    {
        temp = head;
        while(temp->next != nullptr)
        {
            if(temp->getbook_data().getISBN() == b.getISBN() )
            {
                unsigned x = temp->getfreq() + 1;
                temp->setfreq(x);
                number_of_books++;
                return;
            }
            temp = temp->next;
        }
    }
    if(temp->getbook_data().getISBN() == b.getISBN() )
    {
        unsigned x = temp->getfreq() + 1;
        temp->setfreq(x);
        number_of_books++;
    }
    else
    {
        temp->next = new book_node(b);
        number_of_books++;
    }

}
void book_database :: remove_book(book &b)
{
    book_node *prev;
    book_node *current;
    if(head == NULL)
        return;
    else if(head->getbook_data().getISBN() == b.getISBN())
    {
        if(head->getfreq() > 1)
        {
            unsigned x = head->getfreq() - 1;
            head->setfreq(x);
            number_of_books--;
        }

        else
        {
            book_node *temp;
            temp = head;
            head = head->next;
            delete temp;
            number_of_books--;
        }
    }
    else
    {
        prev = head;
        current = head->next;
        while(current!=NULL)
        {
            if(current->getbook_data().getISBN() == b.getISBN())
            {
                if(current->getfreq()>1)
                {
                    unsigned x = current->getfreq() - 1;
                    current->setfreq(x);
                    number_of_books--;
                    break;
                }
                else
                {
                    prev->next = current->next;
                    delete current;
                    number_of_books--;
                    break;
                }
            }
            else
            {
                prev = current;
                current = current->next;
            }
        }
    }

}
bool book_database ::search(string s) {
    if(head == nullptr)
        return false;
    else
    {
        book_node *temp;
        temp = head;
        while(temp != nullptr)
        {
            if(temp->getbook_data().gettitle() == s
               || temp->getbook_data().getISBN() == s)
                return true;
            else
                temp = temp->next;
        }
    }
    return false;
}
book book_database ::get_book_data(string s) {
    // Need to make sure to search for book before getting he book
    book_node *temp;
    temp = head;
    while(temp != nullptr)
    {
        if(temp->getbook_data().gettitle() == s
           || temp->getbook_data().getISBN() == s)
            return temp->getbook_data();
        else
            temp = temp->next;
    }
}
ostream& operator << (ostream &out, book_database &b1)
{
    if(b1.head == nullptr)
        out<<"Book database is empty";
    else
    {
        book_node *temp;
        temp = b1.head;
        while(temp!= nullptr)
        {
            book b1 = temp->getbook_data();
            out<<b1;
            out<<temp->getfreq()<<endl;
            temp = temp->next;
        }
    }
    return out;
}
void book_database :: save_to_file(ostream &out)
{
    book_node *temp;
    temp = head;
    while(temp != nullptr)
    {
        temp->save_to_file(out);
        if(temp->next != nullptr)
            out<<endl;
        temp = temp->next;
    }
}