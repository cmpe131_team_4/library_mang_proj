//
// Created by babbu on 26-04-2018.
//

#ifndef LIB_MANG_PROJECT_V2_BOOK_NODE_H
#define LIB_MANG_PROJECT_V2_BOOK_NODE_H

#include "book.h"
class book_node {
    book book_data;
    unsigned freq;
public:
    book_node *next;
    book_node();
    book_node(book &);
    book getbook_data();
    unsigned getfreq();
    void setfreq(const unsigned &);
    friend ostream& operator << (ostream &,  book_node &);
    void save_to_file(ostream &);
};


#endif //LIB_MANG_PROJECT_V2_BOOK_NODE_H
