//
// Created by babbu on 01-05-2018.
//

#ifndef LIB_MANG_PROJECT_V2_ACCOUNT_DATABASE_H
#define LIB_MANG_PROJECT_V2_ACCOUNT_DATABASE_H

#include "account_node.h"

class account_database {
protected:
    account_node*head;
    int number_of_accounts;
public:
    account_database();
    ~account_database();
    void destroy_accounts();
    void add_account(account &);
    void remove_account(account &);
    bool isEmpty();
    account_node* get_account_node(string id, string password);
    void save_to_file(ostream &);
    friend ostream& operator << (ostream &, account_database &);
};


#endif //LIB_MANG_PROJECT_V2_ACCOUNT_DATABASE_H
