//
// Created by babbu on 27-04-2018.
//

#include "account.h"
#include "date.h"
account :: account ()
{
    user_name = "";
    user_address = "";
    userid = "";
    user_password = "";
    DOB.month = 0;
    DOB.year = 0;
    DOB.day = 0;
    issued_book;
}
account ::account(string name, string address, string id, string password, date d) {

    user_name = name;
    user_address = address;
    userid = id;
    user_password = password;
    DOB = d;
}
string account ::getuser_name() {
    return user_name;
}
string account ::getuser_address() {
    return user_address;
}
string account ::getuserid() {
    return userid;
}
string account :: getuser_password()
{
    return user_password;
}
date account :: getDOB()
{
    return DOB;
}
book account ::getissued_book() {
    return issued_book;
}
void account ::setissued_book(book &b) {
    issued_book = b;
}
istream& operator >> (istream &in, account &a)
{
    cout<<"Enter your name ::";
    std :: getline(in,a.user_name);
    cout<<"Enter your address::";
    std :: getline(in,a.user_address);
    cout<<"Enter your date of birth::"<<endl;
    in>>a.DOB;
    cin.ignore();
    cout<<"Enter your userid::";
    std :: getline(in,a.userid);
    cout<<"Enter the password::";
    std :: getline(in,a.user_password);
    return in;
}
ostream& operator << (ostream &out, const account &a)
{
    out<<a.user_name<<endl;
    out<<a.user_address<<endl;
    out<<a.DOB;
    out<<a.userid<<endl;
    out<<a.user_password<<endl;
    out<<a.issued_book;
    return out;
}
void account :: operator = (const account &account1)
{
    this->issued_book = account1.issued_book;
    this->user_password = account1.user_password;
    this->userid = account1.userid;
    this->user_address = account1.user_address;
    this->user_name = account1.user_name;
    this->DOB = account1.DOB;
}
void account :: read_from_file(istream &in)
{

    getline(in,user_name);
    getline(in,user_address);
    in>>DOB.month;
    in>>DOB.day;
    in>>DOB.year;
    in.ignore();
    getline(in,userid);
    getline(in,user_password);
    issued_book.read_from_file(in);
}
void account ::save_to_file(ostream &out) {
    out<<user_name<<endl;
    out<<user_address<<endl;
    out<<DOB.month<<endl;
    out<<DOB.day<<endl;
    out<<DOB.year<<endl;
    out<<userid<<endl;
    out<<user_password<<endl;
    out<<issued_book.gettitle()<<endl;
    out<<issued_book.getISBN()<<endl;
    out<<issued_book.getauthor_name();
}