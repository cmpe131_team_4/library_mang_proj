//
// Created by babbu on 01-05-2018.
//

#include "account_database.h"
account_database ::account_database() {
    head = nullptr;
    number_of_accounts = 0;
}
account_database ::~account_database() {
    destroy_accounts();
}
void account_database ::destroy_accounts() {
    account_node *temp;
    while(head!= nullptr)
    {
        temp = head;
        head = head->next;
        delete temp;
    }
}
bool account_database :: isEmpty()
{
   if(number_of_accounts == 0)
       return true;
    else
       return false;
}
void account_database :: add_account(account &a){
    if(head == nullptr)
    {
        head = new account_node(a);
        number_of_accounts++;
        return;
    }
    else
    {
        account_node *temp;
        temp = head;
        while(temp->next != nullptr)
            temp = temp->next;
        temp->next = new account_node(a);
    }
}
void account_database ::remove_account(account &a) {
    account_node *prev;
    account_node *current;
    if(head == nullptr)
        return;
    else if(head->getaccount_data().getuserid() == a.getuserid()
            &&
            head->getaccount_data().getuser_password() == a.getuser_password())
    {
        account_node *temp;
        temp = head;
        head = head->next;
        delete temp;
        number_of_accounts--;
    }
    else{
        prev = head;
        current  = head->next;
        while(current != nullptr)
        {
            if(current->getaccount_data().getuserid() == a.getuserid()
                    &&
                    current->getaccount_data().getuser_password() == a.getuser_password())
            {
                prev->next = current->next;
                delete current;
                number_of_accounts--;
                break;
            }

        }
    }
}
account_node* account_database ::get_account_node(string id, string password) {
    if(head == nullptr)
        return nullptr;
    else
    {
        account_node *temp;
        temp = head;
        while(temp != nullptr)
        {
            if(temp->getaccount_data().getuser_password() == password
               || temp->getaccount_data().getuserid() == id)
            {
                break;
            }
            else
                temp = temp->next;
        }
        return temp;
    }

}
ostream &operator << (ostream &out, account_database &a1)
{
    if(a1.head == nullptr)
        out<<"Account Database is empty:"<<endl;
    else
    {
        account_node *temp;
        temp = a1.head;
        while(temp!= nullptr)
        {
            account a1 = temp->getaccount_data();
            out<<a1;
            temp = temp->next;
        }
    }
}
void account_database ::save_to_file(ostream &out) {
    account_node *temp;
    temp =head;
    while(temp != nullptr)
    {
        account a;
        a = temp->getaccount_data();
        a.save_to_file(out);
        if(temp->next != nullptr)
            out<<endl;
        temp = temp->next;

    }
}