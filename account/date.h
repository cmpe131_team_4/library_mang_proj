//
// Created by babbu on 27-04-2018.
//

#ifndef LIB_MANG_PROJECT_V2_DATE_H
#define LIB_MANG_PROJECT_V2_DATE_H

#include <iostream>
using namespace std;
class date {

public:
    unsigned int day;
    unsigned int month;
    unsigned int year;
    //CONSTRUCTORS
    date():day(0),month(0),year(0){}
    date(unsigned int m, unsigned int d,unsigned int y):month(m),day(d),year(y){}
    friend istream& operator >> (istream &, date &);
    friend ostream& operator << (ostream &, const date &);
    void operator = (const date &);
};


#endif //LIB_MANG_PROJECT_V2_DATE_H
