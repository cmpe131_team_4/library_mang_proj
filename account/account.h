//
// Created by babbu on 27-04-2018.
//

#ifndef LIB_MANG_PROJECT_V2_ACCOUNT_H
#define LIB_MANG_PROJECT_V2_ACCOUNT_H

#include <iostream>
#include "book.h"
#include "date.h"
//using namespace std;
class account {
    string user_name;
    string user_address;
    string userid;
    string user_password;
    date DOB;
    book issued_book;
public:
    account();
    account(string , string , string , string , date );
    string getuser_name();
    string getuser_address();
    string getuserid();
    string getuser_password();
    date getDOB();
    book getissued_book();
    void setissued_book(book &);
    friend istream&operator >> (istream &, account &);
    friend ostream&operator << (ostream &, const account &);
    void read_from_file(istream &);
    void operator = (const account &);
    void save_to_file(ostream &);
};

#endif //LIB_MANG_PROJECT_V2_ACCOUNT_H
