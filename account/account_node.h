//
// Created by babbu on 30-04-2018.
//

#ifndef LIB_MANG_PROJECT_V2_ACCOUNT_NODE_H
#define LIB_MANG_PROJECT_V2_ACCOUNT_NODE_H

#include "account.h"

class account_node {
    account account_data;
public:
    account_node *next;
    account_node();
    account_node(account &);
    account getaccount_data();
    void set_issued_book(book &b);
    friend ostream&operator << (ostream &, account_node &);
};


#endif //LIB_MANG_PROJECT_V2_ACCOUNT_NODE_H
