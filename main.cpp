/*
 LAST MODIFIED ON MAY 7, 2018


*/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include "Library.h"
void clearscreen(){system("CLS");}
int login_menu_page();
void create_account(Library &database);
void login_account(Library &database);
int user_menu_page(account_node *user_account);
void user_page(Library &database, account_node *user_account);
void issue_book_to_user(Library &database, account_node *user_account);
void return_book_from_user(Library &database,account_node *user_account);
using namespace std;
int main() {

    Library database;
    //*********************************
    fstream f_book,f_account;
    book b("ISBN2","AUTHOR2","TITLE2");

    f_book.open("books_record.txt",ios::in);
    f_account.open("accounts_record.txt",ios::in);

        if(f_book.fail() || f_account.fail())
        cout<<"Unable to open file:";
    database.read_books_from_file(f_book);
    database.read_accounts_from_file(f_account);
    f_book.close();
    f_account.close();
    //*****************************
    int x;
    do
    {
        x = login_menu_page();
        if(x == 0)
            break;
        else if(x == 1)
            create_account(database);
        else if(x == 2)
            login_account(database);
    }while(x != 0);

    f_book.open("books_record.txt", ios::out);
    f_account.open("accounts_record.txt", ios::out);
    if(f_book.fail() || f_account.fail())
        cout<<"Unable to open file:";
    database.save_books_to_file(f_book);
    database.save_accounts_to_file(f_account);
    f_account.close();
    f_book.close();

    /*
    Library database;
    date d1(0,0,0);

    account a("Prajjwal Singhal", "375 S 9th street" , "011412661", "Pajjwal1",d1);

    database.accounts.add_account(a);
    cout<<database.books;
//    cout<<endl;
//    book b = database.books.get_book_data("ISBN1");
//    cout<<b;
//    account_node *temp;
//    temp = database.get_account_node("011412661","Prajjwal1");
//    if(temp!= nullptr)
//        cout<<temp->getaccount_data();
//    else
//        cout<<"Not available";
//    cout<<database.accounts;
    account_node *temp;
    temp = database.get_account_node("011412661","Prajjwal1");

    database.issue_book(temp,b1);
    database.return_book(temp);
    cout<<temp->getaccount_data();

    cout<<database.books;
     */
    return 0;
}
int login_menu_page()
{
    clearscreen();
    string s;
    do{
        cout<<"MENU :"<<endl;
        cout<<"0. Exit"<<endl;
        cout<<"1. Create New Account:"<<endl;
        cout<<"2. Login :"<<endl;
        cout<<"Enter your choice::";
        getline(cin,s);
        if(s == "0" || s == "1" || s == "2")
            break;
        else
        {
            cout<<"WRONG CHOICE";
            _sleep(1000);
            clearscreen();
        }
    }while(1);
    if(s=="0")
        return 0;
    else if(s=="1")
        return 1;
    else if(s=="2")
        return 2;

}
void create_account(Library &database)
{
    account a;
    cin>>a;
    database.accounts.add_account(a);
}
void login_account(Library &database)
{
    string id, password;
    cout<<"Enter user id ::";
    getline(cin,id);
    cout<<"Enter password::";
    getline(cin,password);
    account_node *user_account;
    user_account = database.accounts.get_account_node(id,password);
    if(user_account == nullptr)
    {
        cout<<"Account not found.";
        _sleep(1000);
        return;
    }
    else
        user_page(database,user_account);
}
void user_page(Library &database, account_node *user_account)
{
    string s;
    do
    {
        int x = user_menu_page(user_account);
        if(x==0)
            break;
        else if(x==1)
            issue_book_to_user(database,user_account);
        else if(x == 2)
            return_book_from_user(database,user_account);
        else if(x == 3)
            cout<<user_account->getaccount_data();
        cout<<"Press any key to go back:";
        getline(cin,s);
    }while(1);


}
int user_menu_page(account_node *user_account)
{
    clearscreen();
    string s;
    do{
        cout<<user_account->getaccount_data().getuser_name()<<endl;
        cout<<"MENU:"<<endl;
        cout<<"0. Exit:"<<endl;
        cout<<"1. Issue book:"<<endl;
        cout<<"2. Return book:"<<endl;
        cout<<"3. View Details:"<<endl;
        cout<<"Enter your choice:";
        getline(cin,s);
        if(s == "0" || s == "1" || s == "2" || s=="3")
            break;
        else
        {
            cout<<"WRONG CHOICE";
            _sleep(1000);
            clearscreen();
        }
    }while(1);
    if(s=="0")
        return 0;
    else if(s=="1")
        return 1;
    else if(s=="2")
        return 2;
    else if(s=="3")
        return 3;

}
void issue_book_to_user(Library &database, account_node *user_account)
{
    clearscreen();
    book temp_book;
    string temp_title;
    temp_book = user_account->getaccount_data().getissued_book();
    temp_title = temp_book.gettitle();
    if(temp_title != "")
    {
        cout<<"Please return previous books before renting new:"<<endl;
        _sleep(3000);
    }
    else
    {
        string temp;
        cout<<"Enter the ISBN no. or the title of the book (case sensitive) :";
        getline(cin,temp);
        if(database.isbookavailable(temp))
        {
            book temp_book;
            temp_book = database.get_book_data(temp);
            database.issue_book(user_account,temp_book);
            cout<<"Book Issued:";
            _sleep(2000);
        }
        else
        {
            cout<<"Book Unavailable:";
            _sleep(2000);
        }
    }
}
void return_book_from_user(Library &database, account_node *user_account)
{
    book temp_book;
    temp_book = user_account->getaccount_data().getissued_book();
    if(temp_book.gettitle() == "")
        cout<<"No books rented yet:";
    else
        database.return_book(user_account);
}