//
// Created by babbu on 27-04-2018.
//

#include "Library.h"

bool Library ::isbookavailable(string s) {
    return books.search(s);
}
account_node* Library ::get_account_node(string id, string password) {
   return accounts.get_account_node(id,password);
}
book Library ::get_book_data(string s) {
    return books.get_book_data(s);
}
void Library ::issue_book(account_node *user_account, book b) {
    user_account->set_issued_book(b);
    books.remove_book(b);
}
void Library :: return_book(account_node *user_account)
{
   book b1,b2;
    b1 = user_account->getaccount_data().getissued_book();
    books.add_book(b1);
    user_account->set_issued_book(b2);

}
void Library:: read_books_from_file(istream &in)
{

    book b;
    string book_frequency;
    while(!in.eof())
    {
        b.read_from_file(in);
        getline(in,book_frequency);
        int frequency = stoi(book_frequency);
        for(int i = 0; i < frequency; i++)
            books.add_book(b);

    }
}
void Library:: save_books_to_file(ostream &out)
{
    books.save_to_file(out);
}
void Library :: read_accounts_from_file(istream &in)
{
    account a;
   // in.ignore();
    while(!in.eof())
    {
        a.read_from_file(in);
        accounts.add_account(a);
    }
}
void Library ::save_accounts_to_file(ostream &out) {
    accounts.save_to_file(out);
}