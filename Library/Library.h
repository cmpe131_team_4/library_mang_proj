//
// Created by babbu on 27-04-2018.
//

#ifndef LIB_MANG_PROJECT_V2_LIBRARY_H
#define LIB_MANG_PROJECT_V2_LIBRARY_H

#include "book_database.h"
#include "account_database.h"
class Library {
public:
    book_database books;
    account_database accounts;
    bool isbookavailable(string );
    account_node* get_account_node(string id, string password);
    book get_book_data(string );
    void issue_book(account_node *user_account, book );
    void return_book(account_node *user_account);
    void read_books_from_file(istream &);
    void save_books_to_file(ostream &);
    void read_accounts_from_file(istream &);
    void save_accounts_to_file(ostream &);

};


#endif //LIB_MANG_PROJECT_V2_LIBRARY_H
